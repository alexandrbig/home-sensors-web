// var { Router, Route, Link, browserHistory } = ReactRouter;
var Router = ReactRouter.Router, 
    Route = ReactRouter.Route, 
    Link = ReactRouter.Link, 
    browserHistory = ReactRouter.browserHistory;
    
var apiRoot = 'https://alexandrbig.asuscomm.com/',
    urls = {
        getSensors: 'sensors',
        getSensorValues: 'sensors/:id/values',
        getGPIOs: 'gpio',
        getGPIOValue: 'gpio/:id',
        setGPIOValue: 'gpio/:id/:value'
    };
var AuthHeader = null;
var auth = localStorage.getItem("gpio-auth");
if(auth) {
    AuthHeader = {'Authorization': auth};
}

var hexToRgb = function(hex) {
    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;

    return r + "," + g + "," + b;
}
var _colors = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"];

var ChartContainer = React.createClass({
    // When the DOM is ready, create the chart.
    componentDidMount: function() {
		// This is for all plots, change Date axis to local timezone
		Highcharts.setOptions({
            global : {
                useUTC : false
            }
        });
      // Extend Highcharts with modules
      if (this.props.modules) {
        this.props.modules.forEach(function(module) {
          module(Highcharts);
        });
		
      }
      // Set container which the chart should render to.
      this.chart = new Highcharts[this.props.type || "Chart"](
        this.props.container,
        this.props.options
      );

      this.chart.showLoading();
      this.props.options.sensor.map(function(sensor, idx){
        this.props.options.getValues(sensor.id).then(function(data){
            if(this.chart && this.chart.series){
                this.chart.hideLoading();
                this.chart.series[idx].setData(data, true);
            }else{
                return;
            }
        }.bind(this))
      }.bind(this))

    },
    //Destroy chart before unmount.
    componentWillUnmount: function() {
        try{
            this.chart.destroy();
        } catch(e){
            
        }
    },
    //Create the div which the chart will be rendered to.
    render: function() {
        return React.createElement('div', {
            id: this.props.container
        });
    }
});

var ChartContainer2 = React.createClass({
	componentDidMount: function() {
		Chart.defaults.global.maintainAspectRatio = false;
		var ctx = document.getElementById(this.props.container);
		var value_unit = this.props.options.sensor[0].value_unit;
		this.refs['chart'].height = 400;
		var myChart = new Chart(this.refs['chart'], {
	    	type: 'line',
/*    		data: {
        		datasets: [{data: []}]
    		},*/
options: {
    scales: {
      xAxes: [
        {
          /*scaleLabel: {
            display: true
          },*/
          type: "time",
          time: {
            unit: "hour",
            displayFormats: {
              hour: "HH:mm:ss"
            }
          },

          position: "bottom"
        }
      ],
		yAxes: [
			{
				labelString: this.props.options.type
			}
		]
    },
	tooltips: {
            callbacks: {
                title: function(tooltipItem, chart) {
                    return moment(tooltipItem[0].xLabel).format('dddd, MMMM Do YYYY, HH:mm:ss');
                },
				label: function(tooltipItem, chart) {
                    return tooltipItem.yLabel + value_unit;
                }
            }
        }

  }
		});

	// http://api.highcharts.com/highcharts/colors
	this.props.options.sensor.map(function(sensor, idx){

        this.props.options.getValues(sensor.id).then(function(data){
			myChart.data.datasets[idx] = {
				data:[], 
				label: sensor.name, 
				backgroundColor: 'rgba(' + hexToRgb(_colors[idx].substr(1)) + ', 0.2)',
            	borderColor: _colors[idx],
			};
			data.map((item) => {
				myChart.data.datasets[idx]['data'].push({x: item[0], y: item[1]});
			});
			myChart.update();
		}.bind(this))
      }.bind(this))


	}, 
	render: function() {
        return React.createElement('div', {
			children: React.createElement('canvas', {
          			  	id: this.props.container,
						ref: 'chart',
        			}),
			height: 400,
			style: {
				position: 'relative'
			}
		})
    }

})
var Charts = React.createClass({

    chartDefaults: {
        chart: {
            type: 'spline'
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                hour: '%H:%M'
            }
        },
        yAxis: [],
        tooltip: {},
        series: [],
		plotOptions: {
			series: {
				fillOpacity: 0.1,
            	/*dataLabels: {
                	enabled: true
            	},
            	enableMouseTracking: false*/
        	}
		},

    },
    sensors: [],
    types: [],

    getInitialState: function() {
        return { types: [] };
    },
    
    getValues: function(id) {
        return fetch(apiRoot + urls.getSensorValues.replace(':id', id))
            .then(function(response) {
                return response.json()
            }).then(function(body) {
                var data = body.map(function(data){
                    var time = +moment(data.time);
					var offset = new Date().getTimezoneOffset() * 60 * 1000 * -1;
                    return [(time + offset), +data.value];
                });
                return data;
            });
    },
    
    componentDidMount: function(){

        fetch(apiRoot + urls.getSensors)
            .then(function(response) {
                return response.json()
            }).then(function(body) {

                this.sensors = body;
                
                var typesTemp = {};
                this.sensors.map(function(sensor, idx){
                    if(!typesTemp[sensor.value_type] && sensor.active == 1){
                        typesTemp[sensor.value_type] = this.types.length + 1;
                        this.types.push({...this.chartDefaults,
                            type: sensor.value_type,
                            yAxis: [{
                                labels:{ format: '{value}' + sensor.value_unit },
                                title: { text: sensor.value_type }
                            }],
                            title: {text :  sensor.value_type},
                            sensor: [sensor], 
                            tooltip: { valueSuffix: sensor.value_unit},
                            series: [{name: sensor.name}],
                            getValues: this.getValues
                        });
                    } else if(sensor.active == 1) {
                        var typeIdx = typesTemp[sensor.value_type] - 1;
                        this.types[typeIdx].sensor.push(sensor);
                        this.types[typeIdx].series.push({name: sensor.name});
                    }
                }.bind(this));
                this.setState({types: this.types});
            }.bind(this))
    },
    
    render: function() {
    
        return (
            <div className="container">
                <h1>Sensors values</h1>
                {this.state.types ? '' : <div style="text-align: center;">Loading, please wait</div>}
                {
                    this.state.types.map(function(type, idx){
                        return React.createElement(ChartContainer, {
                            key: type.type.toString() + idx,
                            container: "chart-"+type.type.toString(),
                            options: type
                        })
                    })
                }
				{ null &&
                    this.state.types.map(function(type, idx){
                        return React.createElement(ChartContainer2, {
                            key: type.type.toString() + idx,
                            container: "chart-"+type.type.toString(),
                            options: type
                        })
                    })
                }

            </div>
        );
    }
});

var Auth = React.createClass({
    getInitialState: function() {
        return { loginStatus: 'undefined' };
    },
    componentDidMount: function(){

    },
    submit: function(e){
        e.preventDefault();
        // @TODO: use proper urlencoded data 
        var data = "username="+document.getElementById('user').value+"&password="+document.getElementById('passw').value;
        this.setState({loginStatus: 'undefined'});
        fetch(apiRoot + urls.getGPIOs, {method: 'post', headers: {...{'Content-Type': "application/x-www-form-urlencoded"}}, body: data})
            .then((response) => {
                if(response.status != 200) {
                    this.setState({loginStatus: 'wrong'});
                    return;
                }
                return response.json();
            }).then((val) => {
                if(val && val.token){
                    AuthHeader = {'Authorization': 'Bearer ' + val.token};
                    localStorage.setItem("gpio-auth", 'Bearer ' + val.token);
                    this.props.onLogin(true);
                }
            }).catch(e => {
                console.warn(e);
            });
    },
    render: function() {

        return (
            <div className="auth">
                { this.state.loginStatus === 'wrong' ? <p className="error">Wrong username or password</p> : ''}
                <form onSubmit={this.submit} name='auth' method='post'>
                    <label htmlFor='user'>Username</label><br/>
                    <input type="text" id='user' name='username' /><br/>
                    <label htmlFor='passw'>Password</label><br/>
                    <input type='password' id='passw' name='password' /><br/>
                    <input type='submit' value='log in'/>
                </form>
            </div>
           
        );
    }
});

var Light = React.createClass({
    gpios: [],
    intervalId: null,
    getInitialState: function() {
        return { gpios: [], isLoggedIn: false };
    },
    componentWillMount: function(){
        if(AuthHeader) this.setState({isLoggedIn: true})
    },
    componentDidMount: function(){
        this.getGpioList();
        if(this.intervalId){
            clearInterval(this.intervalId);
            this.intervalId = null;
        }
        this.intervalId = setInterval(() => { this.checkPinValue(); }, 10000);
    },
    getGpioList: function() {
        this.gpios = [];
        fetch(apiRoot + urls.getGPIOs, {headers:{...AuthHeader}, credentials: 'include'})
            .then((response) => {
                if(this.handleAuth(response.status))
                    return response.json()
            }).then((body) => {
                body.forEach((gpio) => {
                    fetch(apiRoot + urls.getGPIOValue.replace(':id', gpio.id), {headers:{...AuthHeader},credentials: 'include'})
                        .then((response) => {
                            return response.json()
                        }).then((body) => {
                            console.log(body);
                            gpio['value'] = body;
                            this.gpios.push(gpio);
                            this.setState({gpios: this.gpios});
                        });
                });
            }).catch(e => {
                console.warn(e);
            });
    },
    checkPinValue: function(){
        this.gpios.forEach((gpio) => {
            fetch(apiRoot + urls.getGPIOValue.replace(':id', gpio.id), {headers:{...AuthHeader}, credentials: 'include'})
                .then((response) => {
                    if(this.handleAuth(response.status))
                        return response.json()
                }).then((val) => {
                    this.gpios = this.gpios.map((_gpio) => {
                        if(_gpio.id === gpio.id) {
                            _gpio.value = (val == 1 ? true : false)
                        }
                        return _gpio;
                    })
                    this.setState({gpios: this.gpios});
                });
        });
    },
    sendValue: function(id, value){
        fetch(apiRoot + urls.setGPIOValue.replace(':id', id).replace(':value', value ? 1 : 0), {method: 'post', headers:{ ...AuthHeader}, credentials: 'include'})
            .then((response) => {
                if(this.handleAuth(response.status))
                    return response.json()
            }).then((val) => {
                this.gpios = this.gpios.map((gpio) => {
                    if(id === gpio.id) {
                        gpio.value = (val == 1 ? true : false)
                    }
                    return gpio;
                })
                this.setState({gpios: this.gpios});
            });
    },
    handleAuth(status) {
        if(status === 401 || status === 403) {
            AuthHeader = null;
            localStorage.removeItem("gpio-auth");
            this.setState({isLoggedIn: false});
            throw new Error('Unauthorized');
            return false;
        }
        return true;
    },
    componentWillUnmount: function() {
        clearInterval(this.intervalId);
        this.intervalId = null;
    },
    onLogin: function(loginStatus) {
        this.setState({isLoggedIn: loginStatus});
        if(loginStatus) {
            this.getGpioList();
        }
    },
    render: function() {

        return (
            <div className="container">
                <h1>Light control</h1>
                {this.state.isLoggedIn ? 
                    ( !this.state.gpios.length ? <div>Please wait</div> : 
                        this.state.gpios.sort((a, b)=>{ return a.id - b.id;}).map((gpio, idx) => {
                            return <div className={'gpio-button ' + (gpio.value ? 'on' : 'off')} key={'gpio' + idx} onClick={this.sendValue.bind(this, gpio.id, !gpio.value)}><span>{gpio.name} {gpio.value ? 'on' : 'off'}</span></div>
                        }) 
                    )
                    :
                    <Auth onLogin={this.onLogin}/>
                }
            </div>
           
        );
    }
});

var Sensor = React.createClass({
    gpios: [],
    intervalId: null,
    /*getInitialState: function() {
        return { gpios: [], isLoggedIn: false };
    },*/
    componentWillMount: function(){
        //if(AuthHeader) this.setState({isLoggedIn: true})
    },
    componentDidMount: function(){
        //this.getGpioList();
	 if(this.props.type === 'button'){
        if(this.intervalId){
            clearInterval(this.intervalId);
            this.intervalId = null;
        }
        this.intervalId = setInterval(() => { this.checkPinValue(); }, 10000);
	 } else {
	this.prepareSensor();
	}
    },
    getGpioList: function() {
        this.gpios = [];
        fetch(apiRoot + urls.getGPIOs, {headers:{...AuthHeader}, credentials: 'include'})
            .then((response) => {
                if(this.handleAuth(response.status))
                    return response.json()
            }).then((body) => {
                body.forEach((gpio) => {
                    fetch(apiRoot + urls.getGPIOValue.replace(':id', gpio.id), {headers:{...AuthHeader},credentials: 'include'})
                        .then((response) => {
                            return response.json()
                        }).then((body) => {
                            console.log(body);
                            gpio['value'] = body;
                            this.gpios.push(gpio);
                            this.setState({gpios: this.gpios});
                        });
                });
            }).catch(e => {
                console.warn(e);
            });
    },
    checkPinValue: function(){
        this.gpios.forEach((gpio) => {
            fetch(apiRoot + urls.getGPIOValue.replace(':id', gpio.id), {headers:{...AuthHeader}, credentials: 'include'})
                .then((response) => {
                    if(this.handleAuth(response.status))
                        return response.json()
                }).then((val) => {
                    this.gpios = this.gpios.map((_gpio) => {
                        if(_gpio.id === gpio.id) {
                            _gpio.value = (val == 1 ? true : false)
                        }
                        return _gpio;
                    })
                    this.setState({gpios: this.gpios});
                });
        });
    },
    sendValue: function(id, value){
        fetch(apiRoot + urls.setGPIOValue.replace(':id', id).replace(':value', value ? 1 : 0), {method: 'post', headers:{ ...AuthHeader}, credentials: 'include'})
            .then((response) => {
                if(this.handleAuth(response.status))
                    return response.json()
            }).then((val) => {
                this.gpios = this.gpios.map((gpio) => {
                    if(id === gpio.id) {
                        gpio.value = (val == 1 ? true : false)
                    }
                    return gpio;
                })
                this.setState({gpios: this.gpios});
            });
    },
    handleAuth(status) {
        if(status === 401 || status === 403) {
            AuthHeader = null;
            localStorage.removeItem("gpio-auth");
            this.setState({isLoggedIn: false});
            throw new Error('Unauthorized');
            return false;
        }
        return true;
    },
    componentWillUnmount: function() {
        clearInterval(this.intervalId);
        this.intervalId = null;
    },
    onLogin: function(loginStatus) {
        this.setState({isLoggedIn: loginStatus});
        if(loginStatus) {
            this.getGpioList();
        }
    },
    render: function() {

        return (
            <div className="sensor">
		<span className="arrow"/>
                {this.state.value}
		<span className="sensor_name">{this.props.sensor_name}</span>
            </div>
           
        );
    }
});


var Menu = React.createClass({
    // When the DOM is ready, create the chart.
    componentDidMount: function() {},
    render: function() {
        return (
            <nav>
                <Link to='/' activeClassName='active'>Main</Link>
                <Link to='/light' activeClassName='active'>Light</Link>
            </nav>
        )
    }
});

var App = React.createClass({
    // When the DOM is ready, create the chart.
    componentDidMount: function() {},
    render: function() {
        return (
            <div className="app">
                <Menu/>
                {this.props.children}
            </div>
        )
    }
});


ReactDOM.render((

    <Router history={browserHistory}>
        <Route path="" component={App} >
            <Route path="/" component={Charts} />
            <Route path="/light" component={Light} />
            <Route path="/auth" component={Auth} />
        </Route>
    </Router>
), document.getElementById('chart'));
