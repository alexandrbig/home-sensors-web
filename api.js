#!/usr/bin/env node

const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const vhost = require('vhost');

const gpio = require('rpi-gpio');
const Config = require('./config');
const Sensors = require('./sensors');
const gpioConfig = require('./gpio_config');


const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
    function(username, password, done) {
        // database dummy - find user and verify password
        if(username === Config.auth.username && password === Config.auth.password){
            done(null, {
                id: 1,
                firstname: 'gpio',
                lastname: 'user',
                verified: true
            });
        }
        else {
            done(null, false);
        }
    }
));

const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');  
const authenticate = expressJwt({
    secret : Config.auth.secret,
    getToken: function fromHeaderOrQuerystring (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.cookies && req.cookies.gpio && req.cookies.gpio.split(' ')[0] === 'Bearer') { // is valid only for domain
            return req.cookies.gpio.split(' ')[1];
        }
        return null;
    }
});

var apiURLPrefix = Config.api.URLPrefix;
var apiCORSOrigin = Config.api.CORSOrigin;

var availablePins = [];

gpioConfig.forEach(function(pin){
    gpio.setup(pin.id, pin.dir, function(err, val) {
        if(err) return console.error(err);
    });
    availablePins.push(pin.id);
});

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(express['static'](__dirname ));

app.use(passport.initialize()); 

app.use(function(req, res, next) {
    if(apiCORSOrigin.includes(req.headers.origin)) {
       res.header("Access-Control-Allow-Origin", req.headers.origin);    
    }
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});

// Express route for incoming requests for a customer name
app.get(apiURLPrefix + '/sensors/', function(req, res) {
	Sensors.getSensors()
	.then(function(data){
		res.status(200).send(data);
	});
}); 
app.get(apiURLPrefix + '/sensors/:id', function(req, res) {
    Sensors.getSensorById(req.params.id)
	.then(function(data){
		res.status(200).send(data);
	});
}); 
app.get(apiURLPrefix + '/sensors/:id/values', function(req, res) {
    Sensors.getSensorValues(req.params.id, req.query)
	.then(function(data){
		res.status(200).send(data);
	});
}); 
app.get(apiURLPrefix + '/sensors/:id/latest', function(req, res) {
    Sensors.getSensorLatestValues(req.params.id)
	.then(function(data){
		res.status(200).send(data[0]);
	});
}); 


app.get(apiURLPrefix + '/gpio/', authenticate, function(req, res){
    res.status(200).send(gpioConfig);
});

app.post(apiURLPrefix + '/gpio/', passport.authenticate('local', {session: false}), serialize, generateToken, respond);

function serialize(req, res, next) {  
    db.updateOrCreate(req.user, function(err, user){
        if(err) {return next(err);}
        // we store the updated information in req.user again
        req.user = {
            id: user.id
        };
        next();
    });
}

const db = {  
    updateOrCreate: function(user, cb){
        // db dummy, we just cb the user
        cb(null, user);
    }
};

function generateToken(req, res, next) {  
    req.token = jwt.sign({
        id: req.user.id,
    }, 
    Config.auth.secret, 
    {
        expiresIn: +Config.auth.tokenTTL * 60 * 60
    });
    next();
}

function respond(req, res) {
    res.cookie('gpio', req.token, { maxAge: +Config.auth.tokenTTL * 60 * 60 * 1000, httpOnly: true });
    res.status(200).json({
        user: req.user,
        token: req.token
    });
}

app.get(apiURLPrefix + '/gpio/:id/', authenticate, function(req, res) {
    
    if(availablePins.indexOf(+req.params.id) === -1) return res.status(400).send("This pin is not configured");
    gpio.read(req.params.id, function(err, value) {
        if(err) {
            console.error(err);
            res.status(400).send(err);
        } else {
            res.status(200).send(value);
        }
    });
});

app.post(apiURLPrefix + '/gpio/:id/:value', authenticate, function(req, res) {

    if(availablePins.indexOf(+req.params.id) === -1) return res.status(400).send("This pin is not configured");
    
    gpio.write(req.params.id, req.params.value, function(err) {
        if(err) {
            console.error(err);
            res.status(400).send(err);
        } else {
            res.status(200).send(req.params.value)
        }
    });
});

app.get('*', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

// Express route for any other unrecognised incoming requests
app.get(apiURLPrefix + '*', function(req, res) {
    res.status(404).send('Unrecognised API call');
});

// Express route to handle errors
app.use(function(err, req, res, next) {
    if (req.xhr) {
        res.status(500).send('Oops, Something went wrong!');
    } else {
        next(err);
    }
});


// Starting both http & https servers
const httpServer = http.createServer(app);
httpServer.listen(80, () => {
	console.log('HTTP Server running on port 80');
});


var appGlobal = express();

if(Config.server.hosts.length) {
    Config.server.hosts.forEach((host) => appGlobal.use(vhost(host, app)))
}

if (Config.server.https) {
    // Certificate
    const privateKey = Config.server.privateKey && fs.readFileSync(Config.server.privateKey, 'utf8');
    const certificate = Config.server.certificate && fs.readFileSync(Config.server.certificate, 'utf8');
    const ca = Config.server.ca && fs.readFileSync(Config.server.ca, 'utf8');
    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
    };

    const httpsServerAsus = https.createServer(credentials, appGlobal);
    httpsServerAsus.listen(443, () => {
        console.log('HTTPS Server running on port 443');
    });

}

