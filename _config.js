var config = {
    api: {
        URLPrefix: '',// e.g.: '/api'
        CORSOrigin: ['']; // setup CORS Origin header to restrict access by url
    },
    auth: {
        username: 'test',
        password: 'test',
        secret: 'server secret',
        tokenTTL: 72 // in hours
    },
    server: {
        https: true,
        // list of available hosts
        hosts: [],
        // path to key
        privateKey: '',
        // path to certificate
        certificate: '',
        // path to chain
        ca: ''
    }
}
module.exports = config;