Welcome to home sensors API + Web

# What do you get? #
* Small node server which hosts API service which reads sensors data from sqlite db.
* Simple charts web application which uses API service to retrieve sensors values.

# How To? #

* Run 

```
#!bash

sudo ./install.sh
```

* Edit config.js file
* Restart service: 
with systemd:
```
#!bash

sudo systemctl restart sensors.service
```
without systemd:
```
#!bash

sudo /etc/init.d/sensors-service start
```

# NodejS GPG key #

```
curl --silent https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
```

* TODO

# TODO: #

* Add sqlite3 check and install if it is not installed
* Create DB structure
* Add (php)script which writes values to DB