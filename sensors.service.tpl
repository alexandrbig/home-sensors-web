[Unit]
Description=Sensors API
After=network.target

[Service]
ExecStart={dir}/api.js
Restart=always
User=pi
Group=gpio
Environment=PATH=/usr/bin:/usr/local/bin
Environment=NODE_ENV=production
WorkingDirectory={dir}

[Install]
WantedBy=multi-user.target

