#! /bin/sh
# /etc/init.d/sensors-service
#

# Some things that run always
touch /var/lock/sensors-service

export PATH=$PATH:/usr/local/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules

# Carry out specific functions when asked to by the system
case "$1" in
  start)
    echo "Starting script sensors-service "
    exec forever --sourceDir={dir} -p /tmp/forever/ api.js
    ;;
  stop)
    echo "Stopping script sensors-service"
    exec forever stop --sourceDir={dir} api.js
    ;;
  restart)
    echo "Stopping script sensors-service"
    exec forever restart --sourceDir={dir} api.js
    ;;
  *)
    echo "Usage: /etc/init.d/sensors-service {start|stop|restart}"
    exit 1
    ;;
esac

exit 0