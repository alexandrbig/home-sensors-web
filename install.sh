#!/usr/bin/env bash
if ! type -P node 
then
    echo "Installing node"
    curl -sLS https://apt.adafruit.com/add | sudo bash
    apt-get install node
else
    echo "Node is installed, skipping..."
fi

echo "Installing npm modules and deendencies"
npm install -s

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

chmod +x api.js
echo "Copying config"
cp $DIR/_config.js $DIR/config.js
echo "Don't forget to change default values in config.js"

if ! type -P systemctl 
then
    echo "No systemd!!!"
    echo "Installing forever"
    npm install -g -s forever
    echo "Copying service files"
    sed -e "s|{dir}|$DIR|g" sensors-service.tpl > sensors-service
    cp sensors.service /etc/init.d/system
    chmod +x /etc/init.d/sensors-service
    echo "Starting service"
    /etc/init.d/sensors-service start
else
    echo "Copying service files"

    sed -e "s|{dir}|$DIR|g" sensors.service.tpl > sensors.service

    cp sensors.service /etc/systemd/system
    chmod 644 /etc/systemd/system/sensors.service

    echo "Starting service"
    systemctl daemon-reload
    systemctl enable sensors.service
    systemctl start sensors.service
fi

echo "Thank you!"

