var dblite = require('dblite');
var dbPath = '/home/pi/sensors/main.db',
	db = dblite(dbPath);
	
db.query('.headers on');
var tSensors = 'sensors';
var tValueTypes = 'value_types';
var tValues = 'val';
var tLatestValues = 'tmp_val';

module.exports = {
	getSensors: function(){
		console.time('sensors');
		return new Promise(function(resolve, reject){
			db.query(
			'SELECT s.*, vt.value_unit, vt.unit_text, vt.value_type FROM ' + tSensors + ' s ' +
			'LEFT JOIN ' + tValueTypes + ' vt ON vt.id = s.value_type', 
			function(s){
				console.timeEnd('sensors');
				return resolve(s);
			});
		});
	},
	getSensorById: function(id){
		if(!id) return {};
		console.time('sensorsID');
		return new Promise(function(resolve, reject){
			db.query(
			'SELECT s.*, vt.value_unit, vt.unit_text, vt.value_type FROM ' + tSensors + ' s ' +
			'LEFT JOIN ' + tValueTypes + ' vt ON vt.id = s.value_type ' +
			'WHERE s.id = ' + id, 
			function(s){
				console.timeEnd('sensorsID');
				return resolve(s);
			});
		});
	},
	getSensorValues: function(id, params){
		if(!id) return {};
		var dateTo = Date.now();
		var dateFrom = dateTo - 24*60*60* 1000;
		if(params) {
			var milleniumStart = new Date('2000-01-01 00:00:00').getTime();
			if(params.from && params.from > milleniumStart){
				dateFrom = params.from;
			}
			if(params.to && params.to > milleniumStart){
				dateTo = params.to;
				if(!params.from){
					dateFrom = dateTo - 24*60*60* 1000;
				}
			}
			console.log(params);
		}
		// Adapt to unix timestamp
		dateTo = Math.floor(dateTo / 1000);
		dateFrom = Math.floor(dateFrom / 1000);
		
		// var filter = ' AND time >= datetime(' + dateFrom + ', \'unixepoch\', \'localtime\') AND time <= datetime(' + dateTo + ', \'unixepoch\', \'localtime\')';
		var filter = ' AND time >= datetime(' + dateFrom + ', \'unixepoch\', \'localtime\') AND time <= datetime(' + dateTo + ', \'unixepoch\', \'localtime\')';
		console.time('sensorValues' + id);
		return new Promise(function(resolve, reject){
			db.query(
			'SELECT value, time ' + 
			'FROM ' + tValues + ' ' +
			'WHERE sid = ' + id + filter + ' ORDER BY id', 
			function(s){
				console.timeEnd('sensorValues' + id);
				return resolve(s);
			});
		});
	},
	getSensorLatestValues: function(id){
		if(!id) return {};
		console.time('sensorLatest');
		return new Promise(function(resolve, reject){
			db.query(
			'SELECT v.value, v.time, s.name, vt.value_unit, vt.unit_text, vt.value_type ' +
			'FROM ' + tLatestValues + ' v ' +
			'LEFT JOIN ' + tSensors + ' s ON v.sid = s.id ' +
			'LEFT JOIN ' + tValueTypes + ' vt ON vt.id = s.value_type ' +
			'WHERE s.id = ' + id, 
			function(s){
				console.timeEnd('sensorLatest');
				return resolve(s);
			});
		});
	},
	

};

/**
SELECT * FROM val v
			LEFT JOIN sensors s ON v.sid = s.id 
			LEFT JOIN value_types vt ON vt.id = s.value_type
		WHERE time >= datetime($date, 'unixepoch') $query_where $query_end"
*/
