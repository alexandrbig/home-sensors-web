var pins = [
    {
        id: 16,
        dir: 'out',
        name: 'Bedroom Light'
    },
    {
        id: 18,
        dir: 'out',
        name: 'Room Light'
    },
];

module.exports = pins;